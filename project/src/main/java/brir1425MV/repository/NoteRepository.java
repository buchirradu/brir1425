package brir1425MV.repository;

import java.util.List;

import brir1425MV.utils.ClasaException;

import brir1425MV.model.Nota;

public interface NoteRepository {
	
	public void addNota(Nota nota) throws ClasaException;
	public List<Nota> getNote(); 
	public void readNote(String fisier);
	
}
