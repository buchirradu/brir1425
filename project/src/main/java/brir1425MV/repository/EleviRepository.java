package brir1425MV.repository;

import java.util.List;

import brir1425MV.model.Elev;

public interface EleviRepository {
	public void addElev(Elev e);
	public List<Elev> getElevi();
	public void readElevi(String fisier);
}
