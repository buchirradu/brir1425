package brir1425MV.repository;

import brir1425MV.model.Corigent;
import brir1425MV.model.Elev;
import brir1425MV.model.Medie;
import brir1425MV.model.Nota;
import brir1425MV.utils.ClasaException;

import java.util.HashMap;
import java.util.List;

public interface ClasaRepository {
	
	public void creazaClasa(List<Elev> elevi, List<Nota> note);
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa();
	public List<Medie> calculeazaMedii() throws ClasaException;
	public void afiseazaClasa();
	public List<Corigent> getCorigenti();
}
