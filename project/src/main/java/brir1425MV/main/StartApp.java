package brir1425MV.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import brir1425MV.model.Nota;
import brir1425MV.utils.ClasaException;

import brir1425MV.model.Corigent;
import brir1425MV.model.Medie;

import brir1425MV.controller.NoteController;

//functionalitati
//F01.	 adaugarea unei note la o anumita materie (nr. matricol, materie, nota acordata);
//F02.	 calcularea mediilor semestriale pentru fiecare elev (nume, nr. matricol),
//F03.	 afisarea elevilor coringenti, ordonati descrescator dupa numarul de materii la care nu au promovat ?i alfabetic dupa nume.


public class StartApp {

    /**
     * @param args
     * @throws ClasaException
     */
    public static void main(String[] args) throws ClasaException {
        // TODO Auto-generated method stub
        NoteController ctrl = new NoteController();
        List<Medie> medii = new LinkedList<Medie>();
        List<Corigent> corigenti = new ArrayList<Corigent>();
        if (args != null && args.length >= 2) {
            ctrl.readElevi(args[0]);
            ctrl.readNote(args[1]);
            ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        }
        boolean gasit = false;
        while (!gasit) {
            System.out.println("1. Adaugare Nota");
            System.out.println("2. Calculeaza medii");
            System.out.println("3. Elevi corigenti");
            System.out.println("4. Iesire");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                int option = Integer.parseInt(br.readLine());
                switch (option) {
                    case 1:
                        System.out.println("Adauga numar matricol");
                        double numarMatricol = Double.parseDouble(br.readLine());
                        System.out.println("Adauga materie ");
                        String materie = br.readLine();
                        System.out.println("Adauga nota ");
                        double nota = Double.parseDouble(br.readLine());
                        Nota notaAdaugata = new Nota(numarMatricol, materie, nota);
                        ctrl.addNota(notaAdaugata);
                        break;
                    case 2:
                        medii = ctrl.calculeazaMedii();
                        if (medii.isEmpty()) {
                            System.out.println("Nu exista inregistrari!");
                        } else {
                            for (Medie medie : medii)
                                System.out.println(medie);
                        }
                        break;
                    case 3:
                        corigenti = ctrl.getCorigenti();
                        if (corigenti.isEmpty()) {
                            System.out.println(" Nu exista elevi");
                        } else {
                            for (Corigent corigent : corigenti)
                                System.out.println(corigent);
                        }
                        break;
                    case 4:
                        gasit = true;
                        break;
                    default:
                        System.out.println("Introduceti o optiune valida!");
                }

            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
